defmodule LunchWithMeWeb.Schema.GraphIQLTest do
  @moduledoc false
  use LunchWithMeWeb.ConnCase
  import LunchWithMe.Factory
  alias LunchWithMe.AbsintheHelpers
  alias LunchWithMe.Accounts

  describe "GraphIQL" do
    test "Should be up", %{conn: conn} do
      conn =
        conn
        |> put_req_header("accept", "text/html")
        |> get("/api/v1/graphiql")

      assert html_response(conn, 200) =~ "graphiql-workspace"
    end
  end
end
