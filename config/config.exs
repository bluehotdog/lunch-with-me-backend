# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :lunch_with_me,
  ecto_repos: [LunchWithMe.Repo]

config :phoenix, :json_library, Jason
# Configures the endpoint
config :lunch_with_me, LunchWithMeWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "VFw6yHurzZQOhPr+sDpQJVzmBUaAs4j2P+WQb6cL33pj/YEcIhU1aVxkeuVQpoV0",
  render_errors: [view: LunchWithMeWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: LunchWithMe.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# in each environment config file you should overwrite this if it's external
config :lunch_with_me, LunchWithMe.Guardian,
  issuer: "LunchWithMe",
  serializer: LunchWithMe.Guardian

config :lunch_with_me,
  password_hasher: LunchWithMe.Accounts.Argon2PasswordHasher

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.

import_config "#{Mix.env()}.exs"
