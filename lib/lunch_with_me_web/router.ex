defmodule LunchWithMeWeb.Router do
  use LunchWithMeWeb, :router

  pipeline :api do
    plug(Plug.Parsers,
      parsers: [:urlencoded, :multipart, :json, Absinthe.Plug.Parser],
      pass: ["*/*"],
      json_decoder: Poison
    )

    plug(:fetch_session)

    plug(LunchWithMeWeb.Pipelines.JWTAuth)
  end

  scope "/api/v1" do
    pipe_through(:api)

    forward(
      "/graphiql",
      Absinthe.Plug.GraphiQL,
      schema: LunchWithMeWeb.Schema,
      socket: LunchWithMeWeb.UserSocket,
      interface: :advanced,
      pipeline: {ApolloTracing.Pipeline, :plug},
      before_send: {__MODULE__, :absinthe_before_send}
    )

    forward(
      "/",
      Absinthe.Plug,
      schema: LunchWithMeWeb.Schema,
      pipeline: {ApolloTracing.Pipeline, :plug},
      before_send: {__MODULE__, :absinthe_before_send}
    )

    # Below lines are a hack to work with cookies and Absinthe.. only used for signIn/signOut
    def absinthe_before_send(conn, %Absinthe.Blueprint{
          execution: %Elixir.Absinthe.Blueprint.Execution{context: %{sign_out: true}}
        }) do
      LunchWithMe.Guardian.Plug.sign_out(conn)
    end

    def absinthe_before_send(conn, %Absinthe.Blueprint{
          execution: %Elixir.Absinthe.Blueprint.Execution{context: %{auth_token: auth_token}}
        }) do
      {:ok, resource, _claims} = LunchWithMe.Guardian.resource_from_token(auth_token)
      LunchWithMe.Guardian.Plug.sign_in(conn, resource)
    end

    def absinthe_before_send(conn, _), do: conn
  end
end
