defmodule LunchWithMe.Accounts do
  @moduledoc """
  The Accounts context.
  """
  # TODO(danni): there's some issue with dialyzer, its unable to parse correctly the ComeonIn check_pass func
  @dialyzer {:nowarn_function, check_pass: 2}

  import Ecto.Query, warn: false
  alias LunchWithMe.Repo

  alias LunchWithMe.Accounts.User

  @password_hasher Application.get_env(:lunch_with_me, :password_hasher)

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!("1234")
      %User{}

      iex> get_user!("1234")
      ** (Ecto.NoResultsError)

  """
  def get_user!(%{nickname: nickname}), do: Repo.get_by!(User, nickname: nickname)
  def get_user!(%{id: id}), do: Repo.get!(User, id)

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end

  def update_user(user, attrs \\ %{}) do
    user
    |> User.update_changeset(attrs)
    |> Repo.update()
  end

  @doc """
  If the nickname matches the password, returns a {:ok, User} object, else returns {:error, "invalid credentials"}
  """
  def sign_in(nickname, plain_text_pass) do
    User
    |> Repo.get_by(nickname: nickname)
    |> check_pass(plain_text_pass)
  end

  defp check_pass(%User{} = user, plain_text_pass) do
    case @password_hasher.check_pass(user, plain_text_pass) do
      {:ok, user} ->
        {:ok, user}

      {:error, _message} ->
        {:error, "invalid credentials"}
    end
  end

  defp check_pass(nil, _plain_text_pass) do
    @password_hasher.dummy_checkpw()
    {:error, "invalid credentials"}
  end
end
