defmodule LunchWithMe.PasswordHasherStubs do
  import Mox

  def stub_password_hashing do
    LunchWithMe.Accounts.MockPasswordHasher
    |> stub(:hash!, &(&1 <> "1"))
    |> stub(:check_pass, &check_pass/2)
    |> stub(:dummy_checkpw, fn -> false end)

    :ok
  end

  defp check_pass(user, pass) do
    case user do
      nil ->
        {:error, "password dont match"}

      _ ->
        if user.password == pass <> "1" do
          {:ok, user}
        else
          {:error, "password dont match"}
        end
    end
  end
end
