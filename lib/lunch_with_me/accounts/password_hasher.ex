defmodule LunchWithMe.Accounts.PasswordHasher do
  @moduledoc """
  A Behavior to help mock password hashing/check in tests.
  Since password hashing is slow(by design) - We dont want to check it in tests..
  """
  alias LunchWithMe.Accounts.User
  @callback hash!(String.t()) :: binary() | no_return()
  @callback check_pass(%User{}, String.t()) :: {:ok, %User{}} | {:error, String.t()}

  @callback dummy_checkpw() :: boolean
end
