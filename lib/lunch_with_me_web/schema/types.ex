defmodule LunchWithMeWeb.Schema.Types do
  @moduledoc false
  use Absinthe.Schema.Notation

  object :auth_token do
    field(:token, non_null(:string))
  end

  enum(:day_of_week,
    values: [:sunday, :monday, :tuesday, :wednesday, :thursday, :friday, :saturday]
  )

  object :time_range do
    field(:start, non_null(:integer))
    field(:end, non_null(:integer))
  end

  object :day_preference do
    field(:day_of_week, non_null(:day_of_week))
    field(:time_range, non_null(list_of(non_null(:time_range))))
  end

  object :user do
    field(:email, non_null(:string))
    field(:email_verified, non_null(:boolean))
    # name and such
    field(:family_name, non_null(:string))
    field(:given_name, non_null(:string))
    field(:nickname, non_null(:string))

    field(:description, :string)
    field(:day_of_week_pref, list_of(:day_preference))
    # misc
    field(:is_admin, :boolean)
  end

  input_object :update_user_params do
    field(:family_name, :string)
    field(:given_name, :string)
    field(:password, :string)
  end
end
