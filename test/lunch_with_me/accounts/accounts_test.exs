defmodule LunchWithMe.AccountsTest do
  use LunchWithMe.DataCase
  import LunchWithMe.Factory

  alias LunchWithMe.Accounts

  describe "Accounts" do
    test "get_user!/1 returns the user with given id/nickname" do
      user = insert(:user)
      assert Accounts.get_user!(%{nickname: user.nickname}) == user
      assert Accounts.get_user!(%{id: user.id}) == user
    end

    test "create_user/1 fails if password length is less then 8" do
      invalid_pass_user_attrs = Enum.into(%{password: "123456"}, params_for(:user))

      {:error, error_changeset} = invalid_pass_user_attrs |> Accounts.create_user()
      refute error_changeset.valid?

      assert {:password, {"Password must be at least 8 chars long", []}} in error_changeset.errors()
    end

    test "sign_in/2 returns {:ok, user} for correct login" do
      user_attrs = params_for(:user)
      Accounts.create_user(user_attrs)
      assert {:ok, _user} = Accounts.sign_in(user_attrs.nickname, user_attrs.password)
    end

    test "sign_in/2 returns {:error, message} for incorrect nickname or password" do
      user_attrs = params_for(:user)
      Accounts.create_user(user_attrs)

      assert {:error, _message} =
               Accounts.sign_in(user_attrs.nickname <> "a", user_attrs.password)

      assert {:error, _message} =
               Accounts.sign_in(user_attrs.nickname, user_attrs.password <> "a")
    end
  end
end
