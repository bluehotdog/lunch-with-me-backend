use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :lunch_with_me, LunchWithMeWeb.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

config :lunch_with_me, LunchWithMe.Repo,
  username: System.get_env("POSTGRES_USER") || "postgres",
  password: System.get_env("POSTGRES_PASSWORD") || "postgres",
  database: System.get_env("POSTGRES_DB") || "lunch_with_me_test",
  hostname: System.get_env("POSTGRES_HOST") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# helps debugging tests
# ownership_timeout: 999_999

config :lunch_with_me, :password_hasher, LunchWithMe.Accounts.MockPasswordHasher

config :lunch_with_me, LunchWithMe.Guardian, secret_key: "test"
