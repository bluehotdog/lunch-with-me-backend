defmodule LunchWithMeWeb do
  @moduledoc """
  The entrypoint for defining your web interface, such
  as controllers, views, channels and so on.

  This can be used in your application as:

      use LunchWithMeWeb, :controller
      use LunchWithMeWeb, :view

  The definitions below will be executed for every view,
  controller, etc, so keep them short and clean, focused
  on imports, uses and aliases.

  Do NOT define functions inside the quoted expressions
  below. Instead, define any helper function in modules
  and import those modules here.
  """

  def controller do
    quote do
      use Phoenix.Controller, namespace: LunchWithMeWeb
      import Plug.Conn
      alias AppWeb.Router.Helpers, as: Routes
      import LunchWithMeWeb.Gettext
    end
  end

  def view do
    quote do
      use Phoenix.View,
        root: "lib/lunch_with_me_web/templates",
        namespace: LunchWithMeWeb

      # Import convenience functions from controllers
      import Phoenix.Controller, only: [get_flash: 2, view_module: 1]

      alias AppWeb.Router.Helpers, as: Routes
      import LunchWithMeWeb.ErrorHelpers
      import LunchWithMeWeb.Gettext
    end
  end

  def router do
    quote do
      use Phoenix.Router
      import Plug.Conn
      import Phoenix.Controller
    end
  end

  def channel do
    quote do
      use Phoenix.Channel
      import LunchWithMeWeb.Gettext
    end
  end

  @doc """
  When used, dispatch to the appropriate controller/view/etc.
  """
  defmacro __using__(which) when is_atom(which) do
    apply(__MODULE__, which, [])
  end
end
