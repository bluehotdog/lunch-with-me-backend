defmodule LunchWithMeWeb.Plugs.PutResourceToAbsintheContext do
  @moduledoc """
  Tries to read the Bearer token, laod the user resource from it, and put it in the absinthe context
  """
  @behaviour Plug

  def init(opts), do: opts

  def call(conn, _) do
    case Guardian.Plug.current_resource(conn) do
      nil ->
        conn

      user ->
        conn |> Absinthe.Plug.put_options(context: %{current_user: user})
    end
  end
end
