defmodule LunchWithMe.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add(:given_name, :string, null: false)
      add(:family_name, :string, null: false)
      add(:nickname, :string, null: false)
      add(:email, :string, null: false)
      add(:email_verified, :boolean, default: false, null: false)
      add(:password, :string, null: false)
      add(:is_admin, :boolean, null: false, default: false)

      timestamps()
    end

    create(unique_index(:users, [:nickname]))
    create(unique_index(:users, [:email]))
  end
end
