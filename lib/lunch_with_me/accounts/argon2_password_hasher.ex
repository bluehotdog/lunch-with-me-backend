defmodule LunchWithMe.Accounts.Argon2PasswordHasher do
  @moduledoc "Defines a concrete implementation for the PasswordHasher, used in production/staging only"
  @behaviour LunchWithMe.Accounts.PasswordHasher
  # TODO(danni): not sure why dializer reports an error for this function,
  # TODO(danni): make sure to add integration test that actually runs this.
  @dialyzer {:no_return, hash!: 1, dummy_checkpw: 0}

  def hash!(password), do: Argon2.hash_pwd_salt(password)

  def check_pass(user, password),
    do: Argon2.check_pass(user, password, %{hash_key: :password})

  def dummy_checkpw, do: Comeonin.Argon2.dummy_checkpw()
end
