defmodule LunchWithMeWeb.Schema do
  @moduledoc false
  use Absinthe.Schema
  use ApolloTracing
  alias LunchWithMeWeb.Resolvers
  import_types(LunchWithMeWeb.Schema.Types)

  query do
    field :current_user, :user do
      middleware(LunchWithMeWeb.Middlewares.AuthRequired)
      resolve(&Resolvers.Authentication.current_user/2)
    end
  end

  mutation do
    @desc "Recieves a nickname/pass combo and returns a login token that can be used for further API calls"
    field :sign_in_user, :auth_token do
      arg(:password, non_null(:string))
      arg(:nickname, non_null(:string))

      resolve(&Resolvers.Authentication.sign_in/2)

      # Hack to make sure we set the cookies, look at router.ex
      middleware(fn resolution, _ ->
        with %{value: %{token: token}} <- resolution do
          %{resolution | context: Map.put_new(resolution.context, :auth_token, token)}
        end
      end)
    end

    field :sign_out_user, :boolean do
      resolve(fn _, _ ->
        {:ok, true}
      end)

      # Hack to make sure we remove the cookies, look at router.ex
      middleware(fn resolution, _ ->
        %{resolution | context: Map.put_new(resolution.context, :sign_out, true)}
      end)
    end

    field :update_user, :user do
      arg(:user, :update_user_params)

      resolve(&Resolvers.Users.update_user/2)
    end
  end
end
