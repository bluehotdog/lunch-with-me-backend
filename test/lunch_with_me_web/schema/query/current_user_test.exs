defmodule LunchWithMeWeb.Schema.Query.CurrentUserTest do
  @moduledoc false
  use LunchWithMeWeb.ConnCase
  import LunchWithMe.Factory
  alias LunchWithMe.AbsintheHelpers
  alias LunchWithMe.Accounts

  describe "CurrentUser" do
    test "currentUser when given a valid token header returns the user", context do
      user_attrs = params_for(:user)
      {:ok, user} = Accounts.create_user(user_attrs)
      {:ok, token, _claims} = LunchWithMe.Guardian.encode_and_sign(user)

      resp =
        context.conn
        |> put_req_header("authorization", "Bearer #{token}")
        |> current_user_query
        |> json_response(200)
        |> AbsintheHelpers.no_errors!()

      current_user = resp["data"]["currentUser"]
      assert current_user["email"] == user_attrs.email
    end

    test "currentUser returns an unauthorized error when not given a valid token", %{conn: conn} do
      resp =
        conn
        |> recycle()
        |> current_user_query
        |> json_response(200)

      assert resp["data"]["currentUser"] == nil
      errors = Map.get(resp, "errors")
      assert length(errors) == 1

      assert errors |> List.first() |> Map.get("message") ==
               "Auth token required to make this call"
    end
  end

  defp current_user_query(conn) do
    query = """
    {
      currentUser {
        email,
        isAdmin,
        nickname
      }
    }
    """

    conn |> post("/api/v1", AbsintheHelpers.query_skeleton(query, "currentUser"))
  end
end
