defmodule LunchWithMeWeb.Middlewares.AuthRequired do
  @moduledoc false
  @behaviour Absinthe.Middleware

  def call(resolution, _config) do
    case resolution.context do
      %{current_user: _} ->
        resolution

      _ ->
        resolution
        |> Absinthe.Resolution.put_result({:error, "Auth token required to make this call"})
    end
  end
end
