defmodule LunchWithMeWeb.ConnCase do
  @moduledoc """
  This module defines the test case to be used by
  tests that require setting up a connection.

  Such tests rely on `Phoenix.ConnTest` and also
  import other functionality to make it easier
  to build common datastructures and query the data layer.

  Finally, if the test case interacts with the database,
  it cannot be async. For this reason, every test runs
  inside a transaction which is reset at the beginning
  of the test unless the test case is marked as async.
  """
  use ExUnit.CaseTemplate
  import Mox

  using do
    quote do
      # Import conveniences for testing with connections
      use PowerAssert
      use Phoenix.ConnTest
      import LunchWithMeWeb.Router.Helpers

      # The default endpoint for testing
      @endpoint LunchWithMeWeb.Endpoint
    end
  end

  setup :verify_on_exit!

  setup tags do
    :ok = LunchWithMe.PasswordHasherStubs.stub_password_hashing()
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(LunchWithMe.Repo)

    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(LunchWithMe.Repo, {:shared, self()})
    end

    {:ok, conn: Phoenix.ConnTest.build_conn()}
  end
end
