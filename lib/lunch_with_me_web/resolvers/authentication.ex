defmodule LunchWithMeWeb.Resolvers.Authentication do
  @moduledoc false
  # TODO(danni): there's some issue with dialyzer, its unable to parse correctly the ComeonIn check_pass func
  @dialyzer {:nowarn_function, sign_in: 2}
  alias LunchWithMe.Accounts
  @authentication_error "invalid credentials"

  def sign_in(%{nickname: nickname, password: password}, _) do
    case Accounts.sign_in(nickname, password) do
      {:ok, user} ->
        {:ok, token, _} = LunchWithMe.Guardian.encode_and_sign(user)
        {:ok, %{token: token}}

      {:error, _message} ->
        {:error, @authentication_error}
    end
  end

  def current_user(_, %{context: %{current_user: current_user}}) do
    {:ok, current_user}
  end
end
