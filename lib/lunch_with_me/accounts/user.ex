defmodule LunchWithMe.Accounts.User do
  @moduledoc """
  A User schema, we expect the user to have all fields except a profile pic.
  A User can login using his nickname
  """
  use Ecto.Schema
  import Ecto.Changeset

  alias Ecto.Changeset

  @password_hasher Application.get_env(:lunch_with_me, :password_hasher)

  schema "users" do
    field(:email, :string)
    field(:email_verified, :boolean, default: false)
    field(:family_name, :string)
    field(:given_name, :string)
    field(:nickname, :string)
    field(:password, :string)
    field(:is_admin, :boolean, default: false)

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [
      :given_name,
      :family_name,
      :nickname,
      :email,
      :email_verified,
      :password
    ])
    |> validate_required([
      :given_name,
      :family_name,
      :nickname,
      :email,
      :email_verified,
      :password
    ])
    |> unique_constraint(:nickname)
    |> unique_constraint(:email)
    |> validate_password(:password)
    |> put_pass_hash()
  end

  # def make_admin(changeset) do
  #   changeset
  #   |> change(is_admin: true)
  # end
  def update_changeset(user, attrs) do
    user
    |> cast(attrs, [
      :given_name,
      :family_name,
      :password
    ])
    |> validate_required([:nickname])
    |> validate_password(:password)
    |> put_pass_hash()
  end

  defp validate_password(changeset, field, options \\ []) do
    validate_change(changeset, field, fn _, password ->
      case valid_password?(password) do
        {:ok, _} -> []
        {:error, msg} -> [{field, options[:message] || msg}]
      end
    end)
  end

  defp put_pass_hash(%Changeset{valid?: true, changes: %{password: password}} = changeset) do
    change(changeset, password: @password_hasher.hash!(password))
  end

  defp put_pass_hash(changeset), do: changeset

  defp valid_password?(password) when byte_size(password) > 7 do
    {:ok, password}
  end

  defp valid_password?(_), do: {:error, "Password must be at least 8 chars long"}
end
