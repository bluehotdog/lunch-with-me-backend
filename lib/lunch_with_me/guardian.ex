defmodule LunchWithMe.Guardian do
  @moduledoc """
  Guardian implementation, used to encode/decode a resource(in our case a %User) to a token
  """
  use Guardian, otp_app: :lunch_with_me

  alias LunchWithMe.Accounts
  alias LunchWithMe.Accounts.User

  def subject_for_token(%User{} = resource, _claims) do
    {:ok, resource.id}
  end

  def resource_from_claims(claims) do
    # Here we'll look up our resource from the claims, the subject can be
    # found in the `"sub"` key. In `above subject_for_token/2` we returned
    # the resource id so here we'll rely on that to look it up.
    id = claims["sub"]

    case Accounts.get_user!(%{id: id}) do
      nil -> {:error, :resource_not_found}
      user -> {:ok, user}
    end
  end
end
