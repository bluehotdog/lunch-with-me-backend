defmodule LunchWithMe.AbsintheHelpers do
  def query_skeleton(query, query_name) do
    %{
      "operationName" => "#{query_name}",
      "query" => "query #{query_name} #{query}",
      "variables" => "{}"
    }
  end

  def mutation_skeleton(query) do
    %{
      "query" => "mutation #{query}",
      "variables" => "{}"
    }
  end

  def no_errors!(resp) do
    nil = resp["errors"]
    resp
  end
end
