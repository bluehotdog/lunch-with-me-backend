defmodule LunchWithMe.Factory do
  use ExMachina.Ecto, repo: LunchWithMe.Repo

  def user_factory do
    %LunchWithMe.Accounts.User{
      email: Faker.Internet.email(),
      email_verified: true,
      family_name: Faker.Name.last_name(),
      given_name: Faker.Name.first_name(),
      nickname: Faker.Internet.user_name(),
      password: Faker.String.base64(8)
    }
  end
end
