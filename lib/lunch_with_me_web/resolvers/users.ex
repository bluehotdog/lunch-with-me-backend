defmodule LunchWithMeWeb.Resolvers.Users do
  @moduledoc false
  alias LunchWithMe.Accounts

  def update_user(%{user: attrs}, %{context: %{current_user: current_user}}) do
    Accounts.update_user(current_user, attrs)
  end
end
