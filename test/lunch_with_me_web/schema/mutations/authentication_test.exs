defmodule LunchWithMeWeb.Schema.Mutations.AuthenticationTest do
  use LunchWithMeWeb.ConnCase, async: true
  import LunchWithMe.Factory
  alias LunchWithMe.AbsintheHelpers
  alias LunchWithMe.Accounts
  alias LunchWithMe.Guardian.Plug, as: GuardianPlug

  describe "Authentication" do
    setup [:create_user]

    test "sign_in/2 returns a authToken for valid credentials", %{
      user_attrs: user_attrs,
      user: user,
      conn: conn
    } do
      conn = conn |> sign_in(user_attrs.nickname, user_attrs.password)

      token = conn |> json_response(200) |> AbsintheHelpers.no_errors!() |> token_from_resp()
      assert {:ok, claims} = LunchWithMe.Guardian.decode_and_verify(token)
      assert claims["sub"] == user.id
      assert GuardianPlug.current_resource(conn) == user
    end

    test "sign_in/2 returns an error for invalid credentials", %{
      user_attrs: user_attrs,
      conn: conn
    } do
      resp =
        conn
        |> sign_in(user_attrs.nickname, "somePasswordWhichIsNotReally")
        |> json_response(200)

      errors = resp["errors"]

      assert nil == GuardianPlug.current_resource(conn)
      assert length(errors) == 1
      assert hd(errors)["message"] == "invalid credentials"
    end

    test "sign_out/0 removes the user session", %{
      user_attrs: user_attrs,
      user: user,
      conn: conn
    } do
      conn = conn |> sign_in(user_attrs.nickname, user_attrs.password)
      assert GuardianPlug.current_resource(conn) == user

      conn = conn |> sign_out

      assert GuardianPlug.current_resource(conn) == nil
    end
  end

  defp sign_in(conn, nickname, password) do
    mutation = """
    {
      signInUser(nickname: "#{nickname}", password: "#{password}") {
        token
      }
    }
    """

    conn |> post("/api/v1", AbsintheHelpers.mutation_skeleton(mutation))
  end

  defp sign_out(conn) do
    mutation = """
    {
      signOutUser
    }
    """

    conn |> post("/api/v1", AbsintheHelpers.mutation_skeleton(mutation))
  end

  defp token_from_resp(resp) do
    resp["data"]["signInUser"]["token"]
  end

  defp create_user(_context) do
    user_attrs = params_for(:user)
    {:ok, user} = Accounts.create_user(user_attrs)
    %{user_attrs: user_attrs, user: user}
  end
end
