defmodule LunchWithMeWeb.Schema.Mutations.UpdateTest do
  use LunchWithMeWeb.ConnCase, async: true
  import LunchWithMe.Factory
  alias LunchWithMe.AbsintheHelpers
  alias LunchWithMe.Accounts

  describe "update user" do
    setup do
      user_attrs = params_for(:user)
      {:ok, user} = Accounts.create_user(user_attrs)

      {:ok, token, _claims} = LunchWithMe.Guardian.encode_and_sign(user)
      {:ok, token: token, user: user, user_attrs: user_attrs}
    end

    test "update_user/2 changes valid attributes", %{
      conn: conn,
      token: token,
      user_attrs: user_attrs
    } do
      mutation =
        create_mutation(%{
          familyName: "s",
          givenName: "avi",
          password: "12345678"
        })

      resp =
        conn
        |> put_req_header("authorization", "Bearer #{token}")
        |> send_mutation(mutation)
        |> json_response(200)
        |> AbsintheHelpers.no_errors!()

      assert resp["data"]["updateUser"]["familyName"] == "s"
      assert resp["data"]["updateUser"]["givenName"] == "avi"
      assert {:ok, _} = Accounts.sign_in(user_attrs.nickname, "12345678")
    end

    test "update_user/2 doesnt change password if not asked", %{
      conn: conn,
      token: token,
      user_attrs: user_attrs
    } do
      mutation = create_mutation(%{familyName: "s", givenName: "avi"})

      conn
      |> put_req_header("authorization", "Bearer #{token}")
      |> send_mutation(mutation)
      |> json_response(200)
      |> AbsintheHelpers.no_errors!()

      assert {:ok, _} = Accounts.sign_in(user_attrs.nickname, user_attrs.password)
    end

    test "update_user/2 doesnt change invalid attributes", %{
      conn: conn,
      token: token
    } do
      mutation =
        create_mutation(%{
          nickname: "avital",
          familyName: "s",
          givenName: "avi"
        })

      resp =
        conn
        |> put_req_header("authorization", "Bearer #{token}")
        |> send_mutation(mutation)
        |> json_response(200)

      assert resp["errors"] |> hd |> Map.get("message") =~ "invalid"
    end

    defp create_mutation(attrs) do
      attributes =
        attrs
        |> Enum.reduce([], fn {key, value}, acc -> acc ++ [~s(#{key}: "#{value}")] end)
        |> Enum.join(", ")

      """
      {
        updateUser(user: {#{attributes}}){
          givenName,
          nickname,
          familyName
        }
      }
      """
    end

    defp send_mutation(conn, mutation) do
      conn |> post("/api/v1", AbsintheHelpers.mutation_skeleton(mutation))
    end
  end
end
