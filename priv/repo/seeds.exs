# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     LunchWithMe.Repo.insert!(%LunchWithMe.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias LunchWithMe.Accounts.User

admin_user =
  User.changeset(%User{is_admin: true}, %{
    given_name: "admin",
    family_name: "strator",
    email: "admin@example.com",
    nickname: "admin",
    password: "12345678"
  })

LunchWithMe.Repo.insert!(admin_user)
