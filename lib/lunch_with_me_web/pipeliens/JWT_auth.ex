defmodule LunchWithMeWeb.Pipelines.JWTAuth do
  @moduledoc false
  use Guardian.Plug.Pipeline,
    otp_app: :lunch_with_me,
    module: LunchWithMe.Guardian,
    error_handler: LunchWithMe.GuardianErrorHandler

  plug(Guardian.Plug.VerifyHeader, realm: "Bearer")
  plug(Guardian.Plug.VerifySession)
  plug(Guardian.Plug.LoadResource, allow_blank: true)
  plug(LunchWithMeWeb.Plugs.PutResourceToAbsintheContext)
end
